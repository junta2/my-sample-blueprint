import BigNumber from 'bignumber.js';

export class TokenInfo {
  constructor(
    public identifier: string, // token address | pool id (masterchef type)
    public priceUsd: number,
    public amount: BigNumber, // amount divided by token decimals
    public source: string, // price source
  ) {}
}
